%md

### Exercise 1: Build an Eventually Perfect Failure Detector

An Eventually Perfect Failure Detector (EPFD), in Kompics terms,  is a component that **provides** the following port *(already imported in the notebook)*.

    class EventuallyPerfectFailureDetector extends Port {
     indication[Suspect];
     indication[Restore];
    }

Simply put, your component should indicate or 'deliver' to the application the following messages:

    case class Suspect(src: Address) extends KompicsEvent;
    case class Restore(src: Address) extends KompicsEvent;

As you have already learnt from the course lectures, an EPFD, defined in a partially synchronous model, should satisfy the following properties:

1. **Completeness**:  Every process that crashes should be eventually suspected permanently by every correct process
2. **Eventual Strong Accuracy**: No correct process should be eventually suspected by any other correct process
   
To complete this assignment you will have to fill in the missing functionality denoted by the commented sections below and pass the property checking test at the end of this notebook.
The recommended algorithm to use in this assignment is *EPFD with Increasing Timeout and Sequence Numbers*,  which you can find at the second page of this [document](https://courses.edx.org/asset-v1:KTHx+ID2203.1x+2016T3+type@asset+block@epfd.pdf) in the respective lecture.


```scala
import se.kth.edx.id2203.core.Ports._
import se.kth.edx.id2203.validation._
import se.sics.kompics.network._
import se.sics.kompics.sl.{Init, _}
import se.sics.kompics.timer.{ScheduleTimeout, Timeout, Timer}
import se.sics.kompics.{KompicsEvent, Start, ComponentDefinition => _, Port => _}

//Custom messages to be used in the internal component implementation
case class CheckTimeout(timeout: ScheduleTimeout) extends Timeout(timeout);

case class HeartbeatReply(seq: Int) extends KompicsEvent;
case class HeartbeatRequest(seq: Int) extends KompicsEvent;

//Define EPFD Implementation
class EPFD(epfdInit: Init[EPFD]) extends ComponentDefinition {

  //EPFD subscriptions
  val timer = requires[Timer];
  val pLink = requires[PerfectLink];
  val epfd = provides[EventuallyPerfectFailureDetector];

  // EPDF component state and initialization
  
  //configuration parameters
  val self = epfdInit match {case Init(s: Address) => s};
  val topology = cfg.getValue[List[Address]]("epfd.simulation.topology");
  val delta = cfg.getValue[Long]("epfd.simulation.delay");
  
  //mutable state
  var period = cfg.getValue[Long]("epfd.simulation.delay");
  var alive = Set(cfg.getValue[List[Address]]("epfd.simulation.topology"): _*);
  var suspected = Set[Address]();
  var seqnum = 0;

  def startTimer(delay: Long): Unit = {
    val scheduledTimeout = new ScheduleTimeout(period);
    scheduledTimeout.setTimeoutEvent(CheckTimeout(scheduledTimeout));
    trigger(scheduledTimeout -> timer);
  }

  //EPFD event handlers
  ctrl uponEvent {
    case _: Start => handle {
      /* WRITE YOUR CODE HERE  */
      // seqnum, alive set, suspected set, delay(period) are all init in definition
      startTimer(period);
    }
  }

  timer uponEvent {
    case CheckTimeout(_) => handle {
      if (!alive.intersect(suspected).isEmpty) {
        /* WRITE YOUR CODE HERE  */
        period = period + delta;
      }
      
      seqnum = seqnum + 1;
      
      for (p <- topology) {
        if (!alive.contains(p) && !suspected.contains(p)) {
           /* WRITE YOUR CODE HERE  */
           suspected = suspected + p;
           trigger(Suspect(p) -> epfd);
        } else if (alive.contains(p) && suspected.contains(p)) {
          suspected = suspected - p;
          trigger(Restore(p) -> epfd);
        }
        trigger(PL_Send(p, HeartbeatRequest(seqnum)) -> pLink);
      }
      alive = Set[Address]();
      startTimer(period);
    }
  }

  pLink uponEvent {
    case PL_Deliver(src, HeartbeatRequest(seq)) => handle {
      /* WRITE YOUR CODE HERE  */   
      trigger(PL_Send(src, HeartbeatReply(seq)) -> pLink);
    }
    case PL_Deliver(src, HeartbeatReply(seq)) => handle {
      /* WRITE YOUR CODE HERE  */
      if (seq == seqnum || suspected.contains(src)) {
          alive = alive + src;
      }
    }
  }
};

```

# submission
Your submission has been locally simulated and validated.

Click Here to view the output of the simulation.



Correction Results
PASSED	Strong Completeness	✔: Every process that crashes is eventually suspected permanently by every correct process
PASSED	Eventual Strong Accuracy	✔: Eventually, no correct process is suspected by any correct process

Final Comments
**************
Congratulations! Your implementation of the 'Eventually Perfect Failure Detector' satisfies all properties! 
A unique token has been generated for your submission right below. Please do not edit. 

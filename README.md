# ETH I-TOC-2017

---

[TOC]

# ReliableDistAlgs - Part I
## Introduction and Formal Models
* Content of the course
* Lecture 1 Introduction to Distributed Systems
* Software
* Review: Set, Relation and Transition Systems
* Lecture 2 Input Output Automata (optional)
* Programming Exercise 1 
	* Programming Exercise due Jan 16, 2018 16:00 PST This content is graded

## Basic Abstractions and Failure Dectectors
* Lecture 3a Basic Abstractions
* Lecture 3b Model Timing Assumptions and Logical Clocks
* Lecture 4 Failure Detectors
* Graded Quiz Basic Abstractions and Failure Detectors
	* Graded Quiz due Jan 16, 2018 16:00 PST This content is graded
* Programming Exercise 2
	* Programming Exercise due Jan 16, 2018 16:00 PST This content is graded

## Broadcast
* Lecture 5 Broadcast
* Lecture 6 Causal-order Broadcast
* Graded Quiz Broadcast
	* Graded Quiz due Jan 16, 2018 16:00 PST This content is graded
* Programming Exercise 3
	* Programming Exercise due Jan 16, 2018 16:00 PST This content is graded

## Shared Store
* Lecture 7 Shared Memory
* Graded Quiz Shared Memory
	* Graded Quiz due Jan 16, 2018 16:00 PST This content is graded
* Programming Exercise 4
	* Programming Exercise due Jan 16, 2018 16:00 PST This content is graded

## Consensus
* Lecture 8 Consensus
* Lecture 9 Paxos
* Graded Quiz Consensus
	* Graded Quiz due Jan 16, 2018 16:00 PST This content is graded
* Programming Exercise 5
	* Programming Exercise due Jan 16, 2018 16:00 PST This content is graded
